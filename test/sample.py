"""
This is sample test file
"""
# content of test_sample.py

def func(var):
    """ This is sample test file"""
    return var + 1

def test_answer():
    """ This is sample test file"""
    assert func(3) == 4
